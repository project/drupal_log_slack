CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

The Drupal Log Slack module provides notifications about the Drupal log to your
Slack channel via the Slack API.

Features: Real time messaging, search, archiving, screen sharing, file sharing, instant messaging, syncing across all devices, and more.

 * For a full description of the module, visit the project page:
   https://drupal.org/project/drupal_log_slack

 * To submit bug reports and feature suggestions, or to track changes:
   https://drupal.org/project/issues/drupal_log_slack

 * Read more about Slack webhooks:
   https://api.slack.com/incoming-webhooks


REQUIREMENTS
------------

This module requires a Slack webhook and API key. For more information:
 * Slack - https://api.slack.com/incoming-webhooks


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit:
   https://www.drupal.org/node/895232 for further information.


CONFIGURATION
-------------

    1. Navigate to Administration > Modules and enable the module.
    2. Navigate to Administration > Configuration > Development > Drupal Slack
       notification for configuration.
    3. Enter in the Slack Webhook.
    4. Enter a custom footer message.
    5. Enter an optional custom Slack border color, example: (#fffff)
    6. Enter an image URL (optional).
    7. Save configuration.


MAINTAINERS
-----------

Current maintainers:
 * JOY (joy29) - https://www.drupal.org/u/joy29

This project is sponsored by:
 * Srijan Technologies - https://www.drupal.org/srijan-technologies
